var authController = require('../../controllers/auth.controller');
var userController = require('../../controllers/user.controller');

module.exports = function ( router ) {
    router.post('/auth/login', authController.login); 
    router.post('/auth/register', userController.addUser);
    router.get('/auth/validate-token', authController.validate_token);
}