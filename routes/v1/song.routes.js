var multipart = require('connect-multiparty');
var songController = require('../../controllers/song.controller');
var authMiddleware = require('../../middlewares/auth.middleware');
const config = require('config');

var md_upload = multipart({ uploadDir: config.get('upload_dir') });

module.exports = function( router ){
    router.get('/album/:id_album/songs', songController.getSongsByAlbumId);
    router.get('/album/:id_album/song/:id', songController.getSongByAlbumIdAndSongId);
    router.get('/album/:id_album/song/:id/file', songController.serveSongByAlbumIdAndSongId);
    router.post('/album/:id_album/song', [authMiddleware.permit("music-song-create"), md_upload], songController.addSongByAlbumId);
    router.patch('/album/:id_album/song/:id', authMiddleware.permit("music-song-update"), songController.updateSongByAlbumIdAndSongId);
    router.put('/album/:id_album/song/:id', authMiddleware.permit("music-song-update"), songController.updateSongByAlbumIdAndSongId);
    router.delete('/album/:id_album/song/:id', authMiddleware.permit("music-song-delete"), songController.deleteSongByAlbumIdAndSongId);
}