var fs = require('fs');
var path = require('path');
const getMP3Duration = require('get-mp3-duration')
var Song = require('../models/song.model').Song;
const config = require('config');

exports.getSongsByAlbumId = function(req,res){
    Song.find({_album: req.params.id_album}).exec( function( err, data ) {
        if ( err ) {
            res.status(500).send({error: err});
        } else {
            res.send({data: data});
        }
    });
    
}

exports.getSongByAlbumIdAndSongId = function(req,res){
    var populate = [
        {path: "_album", select: {name: 1} }
    ];
    Song.findOne( {_album: req.params.id_album, _id: req.params.id} ).populate( populate ).exec( function(err, data){
        if ( err ) {
            res.status(500).send({error: err});
        } else {
            res.send({data: data});
        }
    });
}

exports.serveSongByAlbumIdAndSongId = function(req,res){

    

    Song.findOne( {_album: req.params.id_album, _id: req.params.id} ).exec( function(err, data){
        if ( err ) {
            res.status(500).send({error: err});
        } else {
            if ( !data ) {
                res.status(404).send({error: 'Song not found'});
            } else {
                var filename = config.get('upload_dir')+data.filename;
                if ( !fs.existsSync(filename) ) {
                    res.status(404).send({error: 'File not found'});
                } else {
                    res.sendFile( path.resolve(filename) );
                }
                //res.send({data: data});
            }
        }
    });
}

exports.addSongByAlbumId = function(req,res){

    
    if ( !req.files || !req.files.song ) {
        res.status(200).send({error: 'Song file is required'});
        return;
    }

    var audio_formats = ['audio/mpeg', 'audio/mp3'];

    if ( audio_formats.indexOf( req.files.song.type ) == -1 ) {
        // remove req.files.song.path
        fs.unlink(req.files.song.path);
        res.status(200).send({error: 'Song type must be mp3'});
        return;
    }

    var buffer = fs.readFileSync(req.files.song.path);
    var ms = getMP3Duration(buffer);


    var song = new Song();

    song.name = req.body.name;
    song.filename = req.files.song.path.replace('uploads/','');
    song.length = (ms/1000).toFixed(0);
    song._album = req.params.id_album;

    song.save( function(err, data){
        if ( err ) {
            res.status(500).send({error: err});
        } else {
            res.send({data: data});
        }
    });
    

}

exports.updateSongByAlbumIdAndSongId = function(req,res){
    // ID del item req.params.id
    var song = {
        name: req.body.name,
        filename: req.body.filename,
        length: req.body.length,
        _album: req.body._album
    }
    Song.findOneAndUpdate({_album: req.params.id_album, _id: req.params.id}, song,{new: true}).exec( function(err, data) {
        if ( err ) {
            res.status(500).send({error: err});
        } else {
            res.send({data: data});
        }
    });

}

exports.deleteSongByAlbumIdAndSongId = function(req,res){
    // ID req.params.id
    Song.findOneAndRemove( { _album: req.params.id_album, _id: req.params.id} ).exec( function(err, data){
        if ( err ) {
            res.status(500).send({error: err});
        } else {
            res.send({data: data});
        }
    });
}